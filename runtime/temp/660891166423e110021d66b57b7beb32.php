<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:73:"I:\phpStudy\WWW\tp5blog\public/../application/index\view\lists\index.html";i:1492665228;s:66:"I:\phpStudy\WWW\tp5blog\public/../application/index\view\base.html";i:1492682629;}*/ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>hdphp<?php echo $_webset['title']; ?>-首页</title>
    <!--描述和摘要-->
    <meta name="Description" content=""/>
    <meta name="Keywords" content=""/>
    <!--载入公共模板-->
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <link rel="stylesheet" type="text/css" href="__STATIC__/index/org/bootstrap-3.3.5-dist/css/bootstrap.min.css" />
    <script src="__STATIC__/index/js/jquery-1.11.3.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="__STATIC__/index/org/bootstrap-3.3.5-dist/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" type="text/css" href="__STATIC__/index/css/index.css" />
    <link rel="stylesheet" type="text/css" href="__STATIC__/index/css/backTop.css"/>
</head>
<body>
<header>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>欢迎来到后盾BLOG教学系统</h1>
            </div>
        </div>
    </div>
</header>
<nav role="navigation" class="navbar navbar-default">
    <div class="container">
        <div class="row">
            <div class="col-sm-12" >

                <div class="navbar-header">

                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse">

                        <span class="sr-only">切换导航</span>

                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>


                <div class="collapse navbar-collapse" id="example-navbar-collapse">
                    <ul class="_menu" >
                        <li condition="!input('param.')"}class="_active"{/if}><a href="/">首页</a></li>
                        <?php if(is_array($_topCate) || $_topCate instanceof \think\Collection || $_topCate instanceof \think\Paginator): if( count($_topCate)==0 ) : echo "" ;else: foreach($_topCate as $key=>$v): ?>
                        <li <?php if(input('param.cate_id')==$v['cate_id']): ?> class="_active"<?php endif; ?>> <a href="<?php echo url('index/lists/index',['cate_id'=>$v['cate_id']]); ?>"><?php echo $v['cate_name']; ?></a></li>
                        <?php endforeach; endif; else: echo "" ;endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>
<section>
    <div class="container">
        <div class="row">
            <!--标签规定文档的主要内容main-->
            <main class="col-md-8">
                
<article>
	<div class="_head category_title">
		<h2>
				<?php echo $headData['title']; ?>：
				<!--分类：-->
			<?php echo $headData['name']; ?>
		</h2>
		<span>
			共 <?php echo $headData['total']; ?> 篇文章
		</span>
	</div>
</article>
<?php if(is_array($articleData) || $articleData instanceof \think\Collection || $articleData instanceof \think\Paginator): if( count($articleData)==0 ) : echo "" ;else: foreach($articleData as $key=>$v): ?>
<article>
	<div class="_head">
		<h1><?php echo $v['arc_title']; ?></h1>
		<span>
		作者：
		<?php echo $v['arc_author']; ?>
		</span>
		•
		<!--pubdate表⽰示发布⽇日期-->
		<time pubdate="pubdate" datetime="<?php echo date('Y-m-d H:i：',$v['sendtime']); ?>"><?php echo date('Y-m-d ',$v['sendtime']); ?></time>
		•
		分类：
		<a href="<?php echo url('index/lists/index',['cate_id'=>$v['cate_id']]); ?>"><?php echo $v['cate_name']; ?></a>
	</div>
	<div class="_digest">
	<img src="<?php echo $v['arc_thumb']; ?>"  class="img-responsive"/>
		<p>
			<?php echo $v['arc_digest']; ?>
		</p>
	</div>
	<div class="_more">
		<a href="<?php echo url('index/content/index',['arc_id'=>$v['arc_id']]); ?>" class="btn btn-default">阅读全文</a>
	</div>
	<div class="_footer">
		<i class="glyphicon glyphicon-tags"></i>
		<?php if(is_array($v['tags']) || $v['tags'] instanceof \think\Collection || $v['tags'] instanceof \think\Paginator): if( count($v['tags'])==0 ) : echo "" ;else: foreach($v['tags'] as $key=>$vo): ?>
		<a href="<?php echo url('index/lists/index',['tag_id'=>$vo['tag_id']]); ?>"><?php echo $vo['tag_name']; ?></a>、
		<?php endforeach; endif; else: echo "" ;endif; ?>
	</div>
</article>
<?php endforeach; endif; else: echo "" ;endif; ?>

            </main>
            <aside class="col-md-4 hidden-sm hidden-xs">
                <div class="_widget">
                    <h4>关于后盾</h4>
                    <div class="_info">
                        <p>最认真的PHP培训机构 只讲真功夫的PHP培训机构 最火爆的IT课程</p>
                        <p>
                            <i class="glyphicon glyphicon-volume-down"></i>
                            <a href="http://www.houdunwang.com" target="_blank">北京后盾网</a>
                        </p>
                    </div>
                </div>
                <div class="_widget">
                    <h4>分类列表</h4>
                    <div>
                        <?php if(is_array($_allCateList) || $_allCateList instanceof \think\Collection || $_allCateList instanceof \think\Paginator): if( count($_allCateList)==0 ) : echo "" ;else: foreach($_allCateList as $key=>$v): ?>
                        <a href="<?php echo url('index/lists/index',['cate_id'=>$v['cate_id']]); ?>"><?php echo $v['cate_name']; ?></a>
                        <?php endforeach; endif; else: echo "" ;endif; ?>
                    </div>
                </div>
                <div class="_widget">
                    <h4>标签云</h4>
                    <div class="_tag">
                        <?php if(is_array($_allTagList) || $_allTagList instanceof \think\Collection || $_allTagList instanceof \think\Paginator): if( count($_allTagList)==0 ) : echo "" ;else: foreach($_allTagList as $key=>$v): ?>
                            <a href="<?php echo url('index/lists/index',['tag_id'=>$v['tag_id']]); ?>"><?php echo $v['tag_name']; ?></a>
                        <?php endforeach; endif; else: echo "" ;endif; ?>
                    </div>
                </div>

            </aside>
        </div>
    </div>
</section>
<footer class="hidden-xs">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <h4 class="_title">最新文章</h4>
                <?php if(is_array($_getNewsArticleDate) || $_getNewsArticleDate instanceof \think\Collection || $_getNewsArticleDate instanceof \think\Paginator): if( count($_getNewsArticleDate)==0 ) : echo "" ;else: foreach($_getNewsArticleDate as $key=>$v): ?>
                <div class="_single">
                    <p><a href="<?php echo url('index/content/index',['arc_id'=>$v['arc_id']]); ?>"><?php echo $v['arc_title']; ?></a></p>
                    <time><?php echo date('Y-m-d H:i:s',$v['sendtime']); ?></time>
                </div>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </div>
            <div class="col-sm-4 footer_tag">
                <div>
                    <h4 class="_title">标签云</h4>
                    <?php if(is_array($_allTagList) || $_allTagList instanceof \think\Collection || $_allTagList instanceof \think\Paginator): if( count($_allTagList)==0 ) : echo "" ;else: foreach($_allTagList as $key=>$v): ?>
                    <a href="<?php echo url('index/lists/index',['tag_id'=>$v['tag_id']]); ?>"><?php echo $v['tag_name']; ?></a>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </div>
            </div>
            <div class="col-sm-4">
                <h4 class="_title">友情链接</h4>
                <div id="" class="_single">
                    <?php if(is_array($_linkDate) || $_linkDate instanceof \think\Collection || $_linkDate instanceof \think\Paginator): if( count($_linkDate)==0 ) : echo "" ;else: foreach($_linkDate as $key=>$v): ?>
                    <p><a href="<?php echo $v['link_url']; ?>" target="_blank"><?php echo $v['link_title']; ?></a></p>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="footer_bottom">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <a href="javascript:void(0);"><?php echo $_webset['title']; ?></a>
                |
                <a href="javascript:void(0);"><?php echo $_webset['copyright']; ?></a>
                |
                <a href="javascript:void(0);"><?php echo $_webset['email']; ?></a>
            </div>
        </div>
    </div>
</div>
<!--返回顶部自己写的插件-->
<script src="__STATIC__/index/js/backTop.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    $(function(){
        //插件使用
        $('.back_top').backTop({right:30});
    })
</script>
<div class="back_top hidden-xs hidden-md">
    <i class="glyphicon glyphicon-menu-up"></i>
</div>
</body>
</html>