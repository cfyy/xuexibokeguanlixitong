/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50547
Source Host           : localhost:3306
Source Database       : tp5blog

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2017-04-25 19:26:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `blog_admin`
-- ----------------------------
DROP TABLE IF EXISTS `blog_admin`;
CREATE TABLE `blog_admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT '' COMMENT '用户名',
  `password` varchar(255) DEFAULT '' COMMENT '密码',
  `login_time` varchar(11) NOT NULL COMMENT '最后登陆时间',
  `login_number` int(11) NOT NULL DEFAULT '0' COMMENT '登陆次数',
  PRIMARY KEY (`admin_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of blog_admin
-- ----------------------------
INSERT INTO `blog_admin` VALUES ('1', 'admin', 'lrt5jZeMovhcO2Pu6i8q2g==', '1493115573', '11');

-- ----------------------------
-- Table structure for `blog_arc_tag`
-- ----------------------------
DROP TABLE IF EXISTS `blog_arc_tag`;
CREATE TABLE `blog_arc_tag` (
  `arc_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  KEY `fk_table1_blog_article1_idx` (`arc_id`),
  KEY `fk_table1_blog_tag1_idx` (`tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文章标签中间表';

-- ----------------------------
-- Records of blog_arc_tag
-- ----------------------------
INSERT INTO `blog_arc_tag` VALUES ('7', '1');
INSERT INTO `blog_arc_tag` VALUES ('7', '5');
INSERT INTO `blog_arc_tag` VALUES ('9', '2');
INSERT INTO `blog_arc_tag` VALUES ('10', '1');
INSERT INTO `blog_arc_tag` VALUES ('10', '2');

-- ----------------------------
-- Table structure for `blog_article`
-- ----------------------------
DROP TABLE IF EXISTS `blog_article`;
CREATE TABLE `blog_article` (
  `arc_id` int(11) NOT NULL AUTO_INCREMENT,
  `arc_title` varchar(45) NOT NULL DEFAULT '' COMMENT '文章标题',
  `arc_author` varchar(45) NOT NULL DEFAULT '' COMMENT '文章作者',
  `arc_digest` varchar(200) NOT NULL DEFAULT '' COMMENT '文章摘要',
  `arc_content` text,
  `sendtime` int(11) NOT NULL DEFAULT '0' COMMENT '发表时间',
  `updatetime` int(11) NOT NULL DEFAULT '0' COMMENT '文章更新时间',
  `arc_click` int(11) NOT NULL DEFAULT '0' COMMENT '点击次数',
  `is_recycle` tinyint(4) NOT NULL DEFAULT '2' COMMENT '是否在回收站，1在回收站2代表不在回收站，默认2',
  `arc_thumb` varchar(100) NOT NULL DEFAULT '' COMMENT '文章缩略图',
  `cate_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `arc_sort` int(11) NOT NULL DEFAULT '100' COMMENT '文章排序',
  PRIMARY KEY (`arc_id`),
  KEY `fk_blog_article_blog_cate_idx` (`cate_id`),
  KEY `fk_blog_article_blog_admin1_idx` (`admin_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='文章表';

-- ----------------------------
-- Records of blog_article
-- ----------------------------
INSERT INTO `blog_article` VALUES ('7', '王者荣耀', '疯行西月', '手游', '<p style=\"text-align: center;\">测试</p><p style=\"text-align: center;\"><img src=\"Http://127.0.0.9/uploads/20170418/401edb60dbeea403ae446903582395fe.png\"/></p>', '1492506654', '1492513722', '1', '2', 'Http://127.0.0.9/uploads/20170418/c4ca2b692b16ff931ebddfe3acd887f8.png', '18', '1', '105');
INSERT INTO `blog_article` VALUES ('9', '分页测试', '12', '234', '<p>斯蒂芬</p>', '1492508398', '1492517300', '1', '2', 'Http://127.0.0.9/uploads/20170418/f609f254073dd959a0b0e95bd30644d0.png', '16', '1', '101');

-- ----------------------------
-- Table structure for `blog_attachment`
-- ----------------------------
DROP TABLE IF EXISTS `blog_attachment`;
CREATE TABLE `blog_attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `filename` varchar(300) NOT NULL COMMENT '文件名',
  `path` varchar(300) NOT NULL COMMENT '路径',
  `extension` varchar(10) NOT NULL DEFAULT '' COMMENT '类型',
  `createtime` int(10) NOT NULL COMMENT '上传时间',
  `size` mediumint(9) NOT NULL COMMENT '文件大小',
  PRIMARY KEY (`id`),
  KEY `extension` (`extension`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='附件';

-- ----------------------------
-- Records of blog_attachment
-- ----------------------------
INSERT INTO `blog_attachment` VALUES ('14', 'QQ截图20170418150511.png', '6e3d598348859990743e1596a0fac05e.png', 'uploads/20170418/6e3d598348859990743e1596a0fac05e.png', 'png', '1492500862', '427656');
INSERT INTO `blog_attachment` VALUES ('15', 'QQ截图20170418150511.png', 'f609f254073dd959a0b0e95bd30644d0.png', 'uploads/20170418/f609f254073dd959a0b0e95bd30644d0.png', 'png', '1492501156', '427656');
INSERT INTO `blog_attachment` VALUES ('16', 'QQ截图20170418150511.png', '401edb60dbeea403ae446903582395fe.png', 'uploads/20170418/401edb60dbeea403ae446903582395fe.png', 'png', '1492501871', '427656');
INSERT INTO `blog_attachment` VALUES ('17', 'QQ截图20170418150511.png', 'c4ca2b692b16ff931ebddfe3acd887f8.png', 'uploads/20170418/c4ca2b692b16ff931ebddfe3acd887f8.png', 'png', '1492501983', '427656');
INSERT INTO `blog_attachment` VALUES ('18', 'QQ截图20170418150511.png', 'f3f33ecbc60a3d835483daef5469f9cc.png', 'uploads/20170418/f3f33ecbc60a3d835483daef5469f9cc.png', 'png', '1492519442', '427656');
INSERT INTO `blog_attachment` VALUES ('19', 'QQ截图20170418150511.png', 'b648e6b5a0feb9fb53f7c24e78013157.png', 'uploads/20170418/b648e6b5a0feb9fb53f7c24e78013157.png', 'png', '1492519548', '427656');
INSERT INTO `blog_attachment` VALUES ('20', 'QQ截图20170418150511.png', 'a8058f21a0576d40b59aab022fa5655f.png', 'uploads/20170418/a8058f21a0576d40b59aab022fa5655f.png', 'png', '1492519657', '427656');
INSERT INTO `blog_attachment` VALUES ('21', 'QQ截图20170418205626.png', '51da301ae3631ca419dd1d813bf0249c.png', 'uploads/20170418/51da301ae3631ca419dd1d813bf0249c.png', 'png', '1492520218', '403');

-- ----------------------------
-- Table structure for `blog_cate`
-- ----------------------------
DROP TABLE IF EXISTS `blog_cate`;
CREATE TABLE `blog_cate` (
  `cate_id` int(11) NOT NULL AUTO_INCREMENT,
  `cate_name` varchar(45) NOT NULL DEFAULT '' COMMENT '分类名称',
  `cate_pid` int(11) NOT NULL DEFAULT '0' COMMENT '分类父id',
  `add_time` varchar(45) NOT NULL DEFAULT '' COMMENT '添加时间',
  `csort` int(11) NOT NULL DEFAULT '10' COMMENT '排序',
  PRIMARY KEY (`cate_id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='分类表';

-- ----------------------------
-- Records of blog_cate
-- ----------------------------
INSERT INTO `blog_cate` VALUES ('15', '在线视频', '0', '1492420600', '1');
INSERT INTO `blog_cate` VALUES ('16', '开发视频', '15', '1492420630', '2');
INSERT INTO `blog_cate` VALUES ('17', '新闻', '0', '1492423793', '1');
INSERT INTO `blog_cate` VALUES ('18', '百度新闻', '17', '1492423809', '2');
INSERT INTO `blog_cate` VALUES ('19', '腾讯新闻', '17', '1492423829', '3');
INSERT INTO `blog_cate` VALUES ('22', 'QQ新闻', '19', '1492429698', '1');

-- ----------------------------
-- Table structure for `blog_link`
-- ----------------------------
DROP TABLE IF EXISTS `blog_link`;
CREATE TABLE `blog_link` (
  `link_id` int(11) NOT NULL AUTO_INCREMENT,
  `link_title` varchar(255) NOT NULL DEFAULT '' COMMENT '链接名称',
  `link_logo` varchar(255) NOT NULL DEFAULT '' COMMENT 'logo',
  `link_url` varchar(255) NOT NULL DEFAULT '' COMMENT '链接地址',
  `link_sort` int(11) NOT NULL DEFAULT '100' COMMENT '链接排序',
  `add_time` varchar(255) NOT NULL DEFAULT '' COMMENT '添加时间',
  PRIMARY KEY (`link_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of blog_link
-- ----------------------------
INSERT INTO `blog_link` VALUES ('2', '小米', 'Http://127.0.0.9/uploads/20170418/51da301ae3631ca419dd1d813bf0249c.png', 'http://www.xiaomi.com', '100', '1492520304');
INSERT INTO `blog_link` VALUES ('3', '百度一下', 'Http://127.0.0.9/uploads/20170418/a8058f21a0576d40b59aab022fa5655f.png', 'http://www.baidu.com', '100', '1492522453');

-- ----------------------------
-- Table structure for `blog_tag`
-- ----------------------------
DROP TABLE IF EXISTS `blog_tag`;
CREATE TABLE `blog_tag` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(255) NOT NULL COMMENT '标签名称',
  PRIMARY KEY (`tag_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of blog_tag
-- ----------------------------
INSERT INTO `blog_tag` VALUES ('1', '新闻');
INSERT INTO `blog_tag` VALUES ('2', '在线视频');
INSERT INTO `blog_tag` VALUES ('5', '嗨氏王者荣耀');

-- ----------------------------
-- Table structure for `blog_webset`
-- ----------------------------
DROP TABLE IF EXISTS `blog_webset`;
CREATE TABLE `blog_webset` (
  `webset_id` int(11) NOT NULL AUTO_INCREMENT,
  `webset_name` varchar(45) NOT NULL DEFAULT '' COMMENT '配置项名称',
  `webset_value` varchar(45) NOT NULL DEFAULT '' COMMENT '配置项值',
  `webset_des` varchar(45) NOT NULL DEFAULT '' COMMENT '配置项描述',
  PRIMARY KEY (`webset_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='站点配置';

-- ----------------------------
-- Records of blog_webset
-- ----------------------------
INSERT INTO `blog_webset` VALUES ('1', 'title', '后盾人 人人做后盾', '网站名称');
INSERT INTO `blog_webset` VALUES ('2', 'email', 'houdunwang@163.com', '站长邮箱');
INSERT INTO `blog_webset` VALUES ('3', 'copyright', 'Copyright @ 2017 后盾网', '版权信息');
