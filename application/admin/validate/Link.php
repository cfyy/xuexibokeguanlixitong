<?php
namespace app\admin\validate;

use think\Validate;

class Link extends  Validate{
    protected $rule = [
        'link_title'  =>  'require',
        'link_logo' =>  'require',
        'link_url' =>  'require',
        'link_sort' =>  'require|between:1,9999',
    ];

    protected $message  =   [
        'link_title.require' => '请输入友链名称',
        'link_logo.require' => '请上传logo',
        'link_url.require' => '请填写url地址',
        'link_sort.require' => '请填写排序',
        'link_sort.between' => '友链排序必须在1~9999之间',
    ];
}