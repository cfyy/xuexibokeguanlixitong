<?php
/**
 * Created by PhpStorm.
 * User: OK
 * Date: 2017/4/17
 * Time: 16:14
 */
namespace app\admin\validate;

use think\Validate;

class Category extends  Validate{
    protected $rule = [
        'cate_name'  =>  'require',
        'csort' =>  'require|number|between:1,9999',
    ];

    protected $message = [
        'cate_name.require'  =>  '请输入栏目名称',
        'csort.require' =>  '请输入排序',
        'csort.number' =>  '排序格式错误',
        'csort.between' =>  '排序必须在1~9999之间',
    ];

    protected $scene = [
        'add'   =>  ['cate_name','csort'],
        'edit'  =>  ['cate_name'],
    ];
}