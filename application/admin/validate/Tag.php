<?php
/**
 * Created by PhpStorm.
 * User: OK
 * Date: 2017/4/17
 * Time: 20:09
 */
namespace app\admin\validate;

use think\Validate;

class Tag extends  Validate{
    protected $rule = [
        'tag_name'  =>  'require',
    ];

    protected $message = [
        'tag_name.require'  =>  '请输入栏目名称',
    ];
}
