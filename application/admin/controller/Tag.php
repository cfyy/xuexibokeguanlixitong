<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;

class Tag extends Common
{

    protected $db;
    protected function _initialize()
    {
        parent::_initialize();
        $this->db = (new \app\admin\model\Tag());
    }

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        $list = db("tag")->paginate(10);
        $this->assign("list",$list);
        return $this->fetch();
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        if(request()->isPost()){
            $result = $this->db->createadd(input('post.'));
            if($result['valid']){
                //操作成功
                $this->success($result['msg'],'admin/tag/index');
            }else{
                //操作失败
                $this->error($result['msg']);
            }
        }
        return $this->fetch();
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $tag_id
     * @return \think\Response
     */
    public function edit($tag_id)
    {
        if(request()->isPost()){
            $result = $this->db->edit(input('post.'));
            if($result['valid']){
                //操作成功
                $this->success($result['msg'],'admin/tag/index');exit;
            }else{
                $this->error($result['msg']);exit;
            }
        }
        if($tag_id){
            $row =  $this->db->where('tag_id',$tag_id)->find();
            $this->assign("row",$row);
        }
        return $this->fetch();
    }

    /**
     * 删除指定资源
     *
     * @param  int  $Tag_id
     * @return \think\Response
     */
    public function del($tag_id)
    {
        if($tag_id){
           $result =  $this->db->del($tag_id);
            if($result['valid']){
                $this->success($result['msg'],'admin/tag/index');exit;
            }else{
                $this->error($result['msg']);exit;
            }
        }else{
            $this->error("缺少主键Id");
        }
    }
}
