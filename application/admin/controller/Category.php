<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;

class Category extends Common
{
    protected $db;
    protected function _initialize()
    {
        parent::_initialize();
        $this->db = (new \app\admin\model\Category());
    }

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        //$list = db("cate")->paginate(10);
        $list = $this->db->getAll();
        $this->assign("list",$list);
        return $this->fetch();
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        if(request()->isPost()){
            $result = $this->db->createadd(input('post.'));
            if($result['valid']){
                //操作成功
                $this->success($result['msg'],'admin/category/index');
            }else{
                //操作失败
                $this->error($result['msg']);
            }
        }
        return $this->fetch();
    }

    /**
     * 添加子栏目
     */
    public function addson(){
        if(request()->isPost()){
            $result = $this->db->createadd(input('post.'));
            if($result['valid']){
                //操作成功
                $this->success($result['msg'],'admin/category/index');exit;
            }else{
                //操作失败
                $this->error($result['msg']);exit;
            }
        }
        $cate_id = input("param.cate_id");
        $row = $this->db->where("cate_id",$cate_id)->find();
        $this->assign("row",$row);
        return $this->fetch();
    }

    /**
     * 编辑
     */
    public function edit(){
        if(request()->isPost()){
            $result = $this->db->edit(input('post.'));
            if($result['valid']){
                //操作成功
                $this->success($result['msg'],'admin/category/index');exit;
            }else{
                $this->error($result['msg']);exit;
            }
        }
        $cate_id = input("param.cate_id");
        $row = $this->db->find($cate_id);
        $cateDate = $this->db->getCateDate($cate_id);
        $this->assign("cateDate",$cateDate);
        $this->assign("row",$row);
        return $this->fetch();
    }

    //删除操作
    public function del(){
        $cate_id = input("get.cate_id");
        //判断是否存在子集，如果存在这删除失败
        if($cate_id){
            $result = $this->db->del($cate_id);
            if($result['valid']){
                $this->success($result['msg'],'admin/category/index');
            }else{
                $this->error($result['msg']);
            }
        }
    }
}
