<?php

namespace app\admin\controller;

use app\admin\model\Admin;
use think\Controller;
use think\Request;

class Login extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        if(session('admin_id')){
            $this->redirect("admin/entry/index");
        }
    }

    /**
     * 显示登陆页面和处理登陆操作
     */
    public function index()
    {
        if(request()->isPost()){
            $res = (new Admin())->login(input('post.'));
            if($res['valid'] == 0){
                //登陆失败
                $this->error($res['msg']);
            }else{
                //登陆成功
                $this->success($res['msg'],'admin/entry/index');
            }
        }
        return $this->fetch('index');
    }
}
