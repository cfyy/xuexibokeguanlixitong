<?php

namespace app\admin\controller;

use think\Controller;

class Webset extends Common
{
    protected $db ;

    protected function _initialize()
    {
        parent::_initialize();
        $this->db = (new \app\admin\model\Webset());
    }

    public function index()
    {
        $list = $this->db->paginate(10);
        $this->assign('list',$list);
        return $this->fetch();
    }

    public function edit()
    {
        if(request()->isAjax()){
           $result = $this->db->edit(input('post.'));
            if($result['valid']){
                $this->success($result['msg'],"index");
            }else{
                $this->error($result['msg']);
            }
        }
    }
}
