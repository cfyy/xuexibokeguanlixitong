<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;

/**
 * 公共类，判断是否登陆了后台操作
 * Class Common
 * @package app\admin\controller
 */
class Common extends Controller
{
    public  function  __construct(Request $request)
    {
        parent::__construct($request);
        if(!session('admin_id')){
            $this->redirect("admin/login/index");
        }
    }
}
