<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;

class Link extends Common
{
    protected $db;
    protected function _initialize()
    {
        parent::_initialize();
        $this->db = (new \app\admin\model\Link());
    }

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        $list = $this->db->getAll();
        $this->assign('list',$list);
        return $this->fetch();
    }

    //添加页面显示
    public function createadd()
    {
        if(request()->isPost()){
            $result = $this->db->createadd(input('post.'));
            if($result['valid']){
                $this->success($result['msg'],'admin/link/index');
            }else{
                $this->error($result['msg']);
            }
        }
        return $this->fetch();
    }

    /**
     * 编辑显示和修改
     * @param $link_id 要修改的id
     */
    public function edit($link_id){
        if(request()->isPost()){
            $result = $this->db->edit(input('post.'));
            if($result['valid']){
                $this->success($result['msg'],'admin/link/index');
            }else{
                $this->error($result['msg']);
            }
        }
        $row = $this->db->where('link_id',$link_id)->find();
        $this->assign('Linkrow',$row);
        return $this->fetch();
    }

    /**
     *根据主键id删除内容
     */
    public function del($link_id){
        $result = $this->db->where('link_id',$link_id)->delete();
        if($result){
            $this->success("删除成功",'admin/link/index');
        }else{
            $this->error("删除失败");
        }
    }
}
