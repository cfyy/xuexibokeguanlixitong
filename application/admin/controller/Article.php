<?php

namespace app\admin\controller;

use app\admin\model\Category;
use think\Controller;
use think\Request;

class Article extends Common
{
    protected $db;
    protected function _initialize()
    {
        parent::_initialize();
        $this->db = (new \app\admin\model\Article());
    }

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        //获取首页显示的数据
        $list = $this->db->getAll(2);
        $this->assign("list",$list);
        return $this->fetch();
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        if(request()->isPost()){
            $result = $this->db->createadd(input('post.'));
            if($result['valid']){
                //操作成功
                $this->success($result['msg'],'admin/Article/index');exit;
            }else{
                //操作失败
                $this->error($result['msg']);exit;
            }
        }
        $catelist = (new Category())->getAll(); //获取分类
        //获取标签
        $taglist = db('tag')->select();
        $this->assign("catelist",$catelist);
        $this->assign("taglist",$taglist);
        return $this->fetch();
    }

    //修改排序
    public function updatesort(){
        if(request()->isAjax()){
            $result = $this->db->updatesort(input('post.'));
            if($result['valid']){
                //操作成功
                $this->success($result['msg'],'admin/Article/index');
            }else{
                //操作失败
                $this->error($result['msg']);
            }
        }
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $arc_id
     * @return \think\Response
     */
    public function edit($arc_id)
    {
        if(request()->isPost()){ //编辑操作
            $result = $this->db->edit(input('post.'));
            if($result['valid']){
                //操作成功
                $this->success($result['msg'],'admin/Article/index');exit;
            }else{
                //操作失败
                $this->error($result['msg']);exit;
            }
        }
        //$arc_id = input("param.arc_id");
        $row = db("article")->where("arc_id",$arc_id)->find();
        //获取分类
        $catelist = (new Category())->getAll();
        //获取标签
        $taglist = db('tag')->select();
        $this->assign("catelist",$catelist);
        $this->assign("taglist",$taglist);
        $this->assign('row',$row);//单条数据
        //获取标签数据
        $tag_ids = db("arc_tag")->where("arc_id",$arc_id)->column("tag_id");
        //halt($tagdata);
        $this->assign("tag_ids",$tag_ids);
        return $this->fetch();
    }

    /**
     * 删除指定资源
     *
     * @param  int  $arc_id
     * @return \think\Response
     */
    public function delete($arc_id)
    {
        $result = $this->db->deleteByArcId($arc_id,1);
        if($result['valid']){
            //操作成功
            $this->success($result['msg'],'admin/Article/index');
        }else{
            //操作失败
            $this->error($result['msg']);
        }
    }

    /**
     * 显示回收列表
     */
    public function recycle(){
        //获取首页显示的数据
        $list = $this->db->getAll(1);
        $this->assign("list",$list);
        return $this->fetch();
    }

    /**
     * 恢复数据
     * @param $arc_id
     */
    public function recovery($arc_id){
        $result = $this->db->deleteByArcId($arc_id,2);
        if($result['valid']){
            //操作成功
            $this->success($result['msg'],'admin/Article/recycle');
        }else{
            //操作失败
            $this->error($result['msg']);
        }
    }

    /**
     * 正真删除数据
     * @param $arc_id 删除id
     */
    public function del($arc_id){
        $result = $this->db->del($arc_id);
        if($result['valid']){
            //操作成功
            $this->success($result['msg'],'admin/Article/recycle');
        }else{
            //操作失败
            $this->error($result['msg']);
        }
    }
}
