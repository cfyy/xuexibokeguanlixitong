<?php

namespace app\admin\controller;

use app\admin\model\Admin;
use think\Controller;
use think\Request;

class Entry extends Common
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        return $this->fetch();
    }

    /**
     *修改密码
     */
    public function updatepwd(){
        if(request()->isPost()){
            $result = (new Admin())->updatepwd(input('post.'));
            if($result['valid']){
                session(null);
                $this->success($result['msg'],'admin/entry/index');exit;
            }else{
                $this->error($result['msg']);exit;
            }
        }
        return $this->fetch();
    }

    /**
     * 退出
     */
    public function loginout(){
        if(session("admin_id")){
            session(null);
            $this->success("退出成功",url('admin/login/index'));exit;
        }else{
            $this->error("退出失败");exit;
        }
    }
}
