<?php

namespace app\admin\model;

use think\Db;
use think\Model;

class Article extends Model
{
    protected $pk = "arc_id";
    protected $table = "blog_article";

    protected $auto = ['admin_id'];
    protected $insert = ['sendtime'];
    protected $update = ['updatetime'];

    protected function setAdminIdAttr($value)
    {
        return session('admin_id');
    }

    protected function setSendTimeAttr($value)
    {
        return time();
    }

    protected function setUpdateTimeAttr($value)
    {
        return time();
    }

    /**
     * 获取所有数据 1回收站2不在回收站
     */
    public function getAll($is_recycle){
      return db('article')
            ->alias('a')
            ->join('__CATE__ c','a.cate_id = c.cate_id')
            ->where('is_recycle',$is_recycle)->field('a.arc_id,a.arc_title,a.arc_author,a.sendtime,a.arc_sort,c.cate_name')
            ->order('a.arc_sort desc,a.sendtime desc,a.arc_id desc')
            ->paginate(10);
    }

    /**
     * 添加文章信息
     * @param $data
     */
    public function createadd($data){
        if(!isset($data['tag_id'])){
            return ['valid' => 0,'msg'=>"请选择标签类型"];
        }
        $result = $this->validate(true)->allowField(true)->save($data);
        if(false === $result){
            // 验证失败 输出错误信息
            return ['valid' => 0,'msg'=>$this->getError()];
        }else{
            //添加中间表标签关系
            if($data['tag_id']){
                $arctagdata = [];
                foreach ($data['tag_id'] as $v){
                    $arctagdata = [
                        'arc_id' =>$this->arc_id,
                        'tag_id' =>$v,
                    ];
                    (new ArcTag())->save($arctagdata);
                }
            }
            return ['valid' => 1,'msg'=>"操作成功"];
        }
    }

    /**
     * ajax修改排序
     * @param $data
     * @return array
     */
    public function updatesort($data){
        $result = $this->validate([
            'arc_sort'  =>  'require|between:1,9999',
        ],[
            'arc_sort.require' =>'请输入排序',
            'arc_sort.between' =>'文章排序必须在1~9999之间',
        ])->save($data,[$this->pk=>$data['arc_id']]);
        if($result){
            return ['valid' => 1,'msg'=>"操作成功"];
        }else{
            return ['valid' => 0,'msg'=>'操作失败'];
        }
    }

    /**
     * 文章编辑
     * @param $data
     */
    public function edit($data)
    {
        //halt($data);
        $result = $this->validate(true)->allowField(true)->save($data,[$this->pk=>$data['arc_id']]);
        if($result){
            return ['valid' => 1,'msg'=>"编辑成功"];
        }else{
            return ['valid' => 0,'msg'=>$this->getError()];
        }
    }

    /**
     * 将文章信息放入回收站 （1回收站2不在回收站）
     */
    public function deleteByArcId($arc_id,$is_recycle){
        $result = $this->save(['is_recycle'=>$is_recycle],[$this->pk=>$arc_id]);
        if($result){
            return ['valid' => 1,'msg'=>"操作成功"];
        }else{
            return ['valid' => 0,'msg'=>$this->getError()];
        }
    }

    /**
     * 删除数据，真实删除
     * @param $arc_id
     * @return array
     */
    public function del($arc_id){
        $result = $this->destroy($arc_id);
        //查询标签表是否存在数据，如果是存在数据，删除，反之。
        $taglist = db('arc_tag')->where('arc_id',$arc_id)->select();
        if(is_array($taglist)){
           $results = (new ArcTag())->where('arc_id',$arc_id)->delete();
        }
        if($result && $results){
            return ['valid' => 1,'msg'=>"操作成功"];
        }else{
            return ['valid' => 0,'msg'=>$this->getError()];
        }
    }
}
