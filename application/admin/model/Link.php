<?php

namespace app\admin\model;

use think\Model;

class Link extends Model
{
    protected $pk = "link_id";
    protected $table = "blog_link";
    protected $insert = ['add_time'];

    protected function setAddTimeAttr($value)
    {
        return time();
    }

    /**
     * 添加友情链接
     * @param $data 要添加的内容
     */
    public function createadd($data){
        $result = $this->validate(true)->save($data);
        if($result){
            return ['valid'=>1,'msg'=>'操作成功'];
        }else{
            return ['valid'=>0,'msg'=>$this->getError()];
        }
    }

    //获取所有内容
    public function getAll(){
        return $this->order('link_sort desc,link_id desc')->paginate(10);
    }

    /**
     * 修改链接内容
     */
    public function edit($data){
        $result = $this->validate(true)->save($data,[$this->pk=>$data['link_id']]);
        if($result){
            return ['valid'=>1,'msg'=>'操作成功'];
        }else{
            return ['valid'=>0,'msg'=>$this->getError()];
        }
    }
}
