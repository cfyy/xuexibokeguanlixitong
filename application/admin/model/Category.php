<?php

namespace app\admin\model;

use houdunwang\arr\Arr;
use think\Model;

class Category extends Model
{


    //主键
    protected $pk = 'cate_id';

    // 设置当前模型对应的完整数据表名称
    protected $table = 'blog_cate';

    /**
     * 添加栏目
     * @param $data
     */
    public function createadd($data){
        $data['add_time'] = time();
        $result = $this->validate(true)->save($data);
        if(false === $result){
            // 验证失败 输出错误信息
            return ['valid' => 0,'msg'=>$this->getError()];
        }else{
            return ['valid' => 1,'msg'=>"添加成功"];
        }
    }

    /**
     * 获取分类数据[树状结构]
     */
    public function getAll(){
        $result = Arr::tree(db('cate')->order('csort desc')->select(), 'cate_name', $fieldPri = 'cate_id', $fieldPid = 'cate_pid');
        return $result;
    }

    /**
     * 处理所属分类
     * @param $cate_id
     */
    public function getCateDate($cate_id){
        //获取cate_id的子集
        $cate_ids = $this->getSon(db('cate')->select(),$cate_id);
        //将自己追加进去
        $cate_ids[] = $cate_id;
        //找到除了子集以外的数据,变成树状结构
        $field = db('cate')->whereNotIn('cate_id',$cate_ids)->select();
        $result = Arr::tree($field, 'cate_name', $fieldPri = 'cate_id', $fieldPid = 'cate_pid');
        return $result;
    }

    /**
     * 查询所有的子集
     * @param $data 所有数据
     * @param $cate_id  分类id
     */
    public function getSon($data,$cate_id){
        static $temp =[];
        foreach ($data as $k=>$v){
            if($cate_id == $v['cate_pid']){
                $temp[] = $v['cate_id'];
                $this->getSon($data,$v['cate_id']);
            }
        }
        return $temp;
    }

    /**
     * 修改更新
     * @param $data  编辑的数据
     */
    public function edit($data){
        $result = $this->validate(true)->save($data,[$this->pk=>$data['cate_id']]);
        if($result){
            return ['valid'=>1,'msg'=>"编辑成功！"];
        }else{
            return ['valid'=>0,'msg'=>$this->getError()];
        }
    }

    /**
     * 删除
     * @param $cate_id 分类id
     */
    public function del($cate_id){
        $result = db('cate')->where("cate_pid",$cate_id)->select();
        if(count($result)){
            return ['valid'=>0,'msg'=>"删除项存在子类，删除失败,请先删除子栏目"];
        }else{
            $this->destroy($cate_id);
            return ["valid"=>1,"msg"=>"删除成功"];
        }
    }
}
