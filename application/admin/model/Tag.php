<?php

namespace app\admin\model;

use think\Model;

class Tag extends Model
{
    protected $pk = "tag_id";
    protected $table = "blog_tag";

    /**
     * 添加成功
     * @param array $data 要添加的数据
     */
    public function createadd($data){
       //验证数据
        $result = $this->validate(true)->save($data);
        if(false === $result){
            // 验证失败 输出错误信息
            return ['valid' => 0,'msg'=>$this->getError()];
        }else{
            return ['valid' => 1,'msg'=>"添加成功"];
        }
    }

    public function edit($data){
        $result = $this->validate(true)->save($data,[$this->pk=>$data['tag_id']]);
        if($result){
            return ['valid'=>1,'msg'=>"编辑成功！"];
        }else{
            return ['valid'=>0,'msg'=>$this->getError()];
        }
    }

    /**
     * 删除操作
     * @param $data
     */
    public function del($tag_id){
        if($tag_id){
            $this->where("tag_id",$tag_id)->delete();
            return ["valid"=>1,"msg"=>"删除成功！"];
        }else{
            return ["valid"=>0,"msg"=>"参数错误"];
        }
    }
}
