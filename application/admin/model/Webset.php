<?php

namespace app\admin\model;

use think\Model;

class Webset extends Model
{
    protected $pk = "webset_id";
    protected $table = "blog_webset";
    //修改内容
    public function edit($data){
        $result = $this->validate([
            'webset_value'=>"require",
        ],[
            'webset_value.require'=>"请输入站点配置值",
        ])->save($data,[$this->pk=>$data['webset_id']]);
        if($result){
            return ['valid'=>"1",'msg'=>"操作成功"];
        }else{
            return ['valid'=>"0",'msg'=>"操作失败"];
        }
    }
}
