<?php

namespace app\index\controller;

use think\Controller;
use think\Request;

/**
 * 获取公共信息
 * Class Common
 * @package app\index\controller
 */
class Common extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        //1.获取顶级分类
        $topCate = $this->getCateByTop();
        $this->assign("_topCate",$topCate);

        //2.获取全部的分类列表
        $allCateList = $this->GetCateDataAllList();
        $this->assign("_allCateList",$allCateList);

        //3获取标签云
        $allTagList = $this->gettaglist();
        $this->assign("_allTagList",$allTagList);
        //4获取最新文章
        $getNewsArticleDate = $this->getNewsArticleDate();
        $this->assign('_getNewsArticleDate',$getNewsArticleDate);
        //5友情链接
        $linkDate = $this->getLink();
        $this->assign("_linkDate",$linkDate);
        //6读取配置信息
        $webset = $this->loadWebSet();
        $this->assign("_webset",$webset);
        //halt($webset);
    }

    /**
     * 读取配置项
     */
    private function loadWebSet()
    {
        return db('webset')->column('webset_value','webset_name');
    }

    /**
     * 获取友情链接
     */
    private function getLink(){
        return db('link')->field('link_url,link_title')->select();
    }

    /**
     * 获取顶级分类
     */
    private function GetCateByTop(){
        return db('cate')->where('cate_pid',0)->select();
    }

    /**
     * 获取全部的分类列表
     */
    private function  GetCateDataAllList(){
        return db("cate")->order('csort desc')->select();
    }

    /**
     * 获取标签云
     */
    private  function  gettaglist(){
        return db('tag')->select();
    }

    /**
     * 获取最新文章
     */
    private function getNewsArticleDate(){
        return db('article')->where('is_recycle',2)->order('sendtime desc')->field('arc_id,arc_title,sendtime')->limit(2)->select();
    }
}
