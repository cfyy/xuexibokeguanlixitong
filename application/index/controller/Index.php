<?php
namespace app\index\controller;

class Index extends Common
{
    public function index()
    {
        $indexNewsList = $this->getIndexNewsList();
        $this->assign("indexNewsList",$indexNewsList);
        //halt($indexNewsList);
        return $this->fetch();
    }

    //获取首页信息
    private function getIndexNewsList(){
        $newslist = db("article")->alias('a')
            ->join('__CATE__ c','a.cate_id=c.cate_id')->where('is_recycle',2)->order('a.sendtime desc')->select();
        foreach ($newslist as $k=>$v){
            $newslist[$k]['tag'] = db('arc_tag')->alias('a')->join('__TAG__ t','a.tag_id=t.tag_id')->where('a.arc_id',$v['arc_id'])->field('t.tag_id,t.tag_name')->select();
        }
        return $newslist;
    }
}
