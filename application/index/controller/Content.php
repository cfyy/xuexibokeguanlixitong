<?php

namespace app\index\controller;

class Content extends Common
{
    public function index(){
        $arc_id =  input('param.arc_id');
        db('article')->where('arc_id',$arc_id)->setInc('arc_click');
        $content = db('article')->find($arc_id);
        $content['tag'] = db("arc_tag")->alias('a')->join('__TAG__ t','a.tag_id=t.tag_id')->where('a.arc_id',$arc_id)->field('t.tag_id,t.tag_name')->select();
        //halt($content);
        $this->assign("contentData",$content);
        return $this->fetch();
    }
}
